import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.calculate(expression));

        expression = "100+300+4";
        assertEquals(404, calculatorResource.calculate(expression));

        expression = " 300 - 99 ";
        assertEquals(201, calculatorResource.calculate(expression));

        expression = " 300 - 99 - 100";
        assertEquals(101, calculatorResource.calculate(expression));

        expression = "3 * 3";
        assertEquals(9,calculatorResource.calculate(expression));

        expression = "3 * 3 * 3";
        assertEquals(27,calculatorResource.calculate(expression));

        expression = "12 / 3";
        assertEquals(4,calculatorResource.calculate(expression));

        expression = "12 / 3 / 2";
        assertEquals(2,calculatorResource.calculate(expression));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "300+99";
        assertNotEquals(400, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

        expression = "20-2";
        assertNotEquals(50, calculatorResource.subtraction(expression));
    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "12*12";
        assertEquals(144,calculatorResource.multiplication(expression));

        expression = "3*3";
        assertNotEquals(8,calculatorResource.multiplication(expression));
    }
}
